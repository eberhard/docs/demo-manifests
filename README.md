# Demo Manifest Repository

Example repository to demonstrate how a Kustomization-based manifest repo would look like.

## Repository structure

### `base/`

This folder contains the base Kustomization. In theory, this could also live in a separate repository alltogether.


### `overlay/`

The actual deployments. Each subfolder is a Kustomization that includes the base as well as environement-specifc changes.
